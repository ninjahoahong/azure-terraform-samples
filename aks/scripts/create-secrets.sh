kubectl create secret -n $1 generic secret \
    --from-literal=data-host=${DATA_HOST:-localhost} \
    --from-literal=data-user=${DATA_USER:-user} \
    --from-literal=data-password=${DATA_PASSWORD:-123} \
    --from-literal=jwt-secret=${JWT_SECRET:-jwt}
