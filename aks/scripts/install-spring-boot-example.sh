#!/bin/bash

# Install nginx ingress
kubectl create ns ingress-basic
helm repo update
helm fetch stable/nginx-ingress --untar
helm template nginx-ingress \
    --name nginx \
    --namespace ingress-basic \
    --set controller.replicaCount=2 \
    --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux \
    --set defaultBackend.nodeSelector."beta\.kubernetes\.io/os"=linux | kubectl -n ingress-basic apply -f -

# Install the CustomResourceDefinition resources separately
kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.8/deploy/manifests/00-crds.yaml

# Install cert-manager
kubectl create namespace cert-manager
kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true

helm repo add jetstack https://charts.jetstack.io
helm repo update
helm fetch jetstack/cert-manager --untar
# Install the cert-manager Helm chart
helm template cert-manager \
  --name cert-manager \
  --namespace cert-manager \
  --set version="0.9.0" | kubectl -n cert-manager apply -f -

# Install cluster-issuer
kubectl apply -f cluster-issuer.yaml
kubectl apply -f tls-secret.yml


