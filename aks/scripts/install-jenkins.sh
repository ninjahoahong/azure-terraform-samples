#!/bin/bash

helm init -c 
kubectl apply -f jenkins-volume.yml
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update
helm fetch stable/jenkins --untar
kubectl create ns jenkins
helm template jenkins --name jenkins-deployment --namespace jenkins | kubectl -n jenkins apply -f -

