output "psql_data_host_url" {
  value = "${azurerm_postgresql_server.psql_server.name}.postgres.database.azure.com"
}

output "psql_data_admin_login_username" {
  value = "${azurerm_postgresql_server.psql_server.administrator_login}@${azurerm_postgresql_server.psql_server.name}"
  sensitive = true
}

output "psql_admin_user_password" {
  value     = azurerm_postgresql_server.psql_server.administrator_login_password
  sensitive = true
}

output "client_key" {
  value = azurerm_kubernetes_cluster.aks.kube_config.0.client_key
  sensitive = true
}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate
  sensitive = true
}

output "cluster_ca_certificate" {
  value = azurerm_kubernetes_cluster.aks.kube_config.0.cluster_ca_certificate
  sensitive = true
}

output "cluster_username" {
  value = azurerm_kubernetes_cluster.aks.kube_config.0.username
}

output "cluster_password" {
  value = azurerm_kubernetes_cluster.aks.kube_config.0.password
  sensitive = true
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.aks.kube_config_raw
  sensitive = true
}

output "host" {
  value = azurerm_kubernetes_cluster.aks.kube_config.0.host
}

