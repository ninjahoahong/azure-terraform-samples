variable "subscription_id" {
}

variable "location" {
  description = "The location/region where the core network will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
}

variable "project" {
}

variable "tags" {
  type = map(string)

  default = {
    environment = "dev"
    terraform   = "true"
    owner       = "ninjahoahong"
  }
}

variable "keyvault_name" {
  description = "name need to match pattern ^[a-zA-Z0-9-]{3,24}$"
}

variable "keyvault_tenant_id" {

}

variable "keyvault_object_id" {
  
}

variable "psql_admin_login_user" {

}

variable "psql_version" {
  
}

variable "psql_sku_name" {
  
}

variable "psql_sku_capacity" {

}

variable "psql_sku_tier" {

}

variable "psql_sku_family" {

}

variable "psql_ssl_enforcement" {

}

variable "psql_storage_mb" {

}

variable "psql_backup_retention_days" {

}

variable "psql_geo_redundant_backup" {

}

variable "acr_name" {

}

variable "acr_sku" {

}

variable "acr_admin_enabled" {

}

variable "aks_principal_client_id" {

}

variable "aks_principal_client_secret" {

}

variable "aks_agent_pool_name" {

}

variable "aks_agent_pool_count" {

}

variable "aks_agent_pool_vm_size" {

}

variable "aks_agent_pool_os_type" {

}

variable "aks_agent_pool_os_disk_size_gb" {

}

