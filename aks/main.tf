provider "azurerm" {
  version         = "=1.28.0"
  subscription_id = var.subscription_id
}

resource "azurerm_resource_group" "resource_group" {
  name     = "${var.project}-rg"
  location = var.location
  tags     = var.tags
}

resource "azurerm_key_vault" "keyvault" {
  name                        = var.keyvault_name
  location                    = azurerm_resource_group.resource_group.location
  resource_group_name         = azurerm_resource_group.resource_group.name
  enabled_for_disk_encryption = true
  tenant_id                   = var.keyvault_tenant_id
  tags                        = var.tags
  sku {
    name = "standard"
  }
}

resource "azurerm_key_vault_access_policy" "keyvault_access_policy" {
  key_vault_id = azurerm_key_vault.keyvault.id

  tenant_id = var.keyvault_tenant_id
  object_id = var.keyvault_object_id

  key_permissions = [
    "create",
    "get",
  ]

  secret_permissions = [
    "set",
    "get",
    "delete",
  ]
}

resource "azurerm_container_registry" "azure_acr" {
  name                     = var.acr_name
  resource_group_name      = azurerm_resource_group.resource_group.name
  location                 = azurerm_resource_group.resource_group.location
  sku                      = var.acr_sku
  admin_enabled            = var.acr_admin_enabled
  tags                     = var.tags
}

resource "azurerm_kubernetes_cluster" "aks" {
  name                = "${var.project}-aks"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name
  dns_prefix          = "${var.project}-aks-dns"
  tags                = var.tags

  agent_pool_profile {
    name            = var.aks_agent_pool_name
    count           = var.aks_agent_pool_count
    vm_size         = var.aks_agent_pool_vm_size
    os_type         = var.aks_agent_pool_os_type
    os_disk_size_gb = var.aks_agent_pool_os_disk_size_gb
  }

  service_principal {
    client_id     = var.aks_principal_client_id
    client_secret = var.aks_principal_client_secret
  }
}

resource "random_string" "psql_admin_login_password" {
  length = 32
  special = true
  min_upper   = 1
  min_lower   = 1
  min_numeric = 1
  min_special = 1
}

resource "azurerm_postgresql_server" "psql_server" {
  name                = "${var.project}-psql-server"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  tags                = var.tags

  
  sku {
    #[B_Gen4_1 B_Gen4_2 B_Gen5_1 B_Gen5_2 GP_Gen4_2 GP_Gen4_4 GP_Gen4_8 GP_Gen4_16 GP_Gen4_32 GP_Gen5_2 GP_Gen5_4 GP_Gen5_8 GP_Gen5_16 GP_Gen5_32 GP_Gen5_64 MO_Gen5_2 MO_Gen5_4 MO_Gen5_8 MO_Gen5_16 MO_Gen5_32]
    name     = var.psql_sku_name
    capacity = var.psql_sku_capacity
    #[Basic GeneralPurpose MemoryOptimized]
    tier     = var.psql_sku_tier
    family   = var.psql_sku_family
  }

  storage_profile {
    storage_mb            = var.psql_storage_mb
    backup_retention_days = var.psql_backup_retention_days
    geo_redundant_backup  = var.psql_geo_redundant_backup
  }

  administrator_login          = var.psql_admin_login_user
  administrator_login_password = random_string.psql_admin_login_password.result
  version                      = var.psql_version
  ssl_enforcement              = var.psql_ssl_enforcement

  lifecycle {
    ignore_changes = ["administrator_login_password"]
  }
}

resource "azurerm_postgresql_firewall_rule" "access_azure_services" {
  name                = "internal"
  resource_group_name = azurerm_resource_group.resource_group.name
  server_name         = azurerm_postgresql_server.psql_server.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}

# # Kubernetes configuration
# provider "kubernetes" {
#   host = azurerm_kubernetes_cluster.aks.kube_config.0.host

#   client_certificate     = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate)
#   client_key             = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_key)
#   cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.cluster_ca_certificate)
# }

# resource "kubernetes_pod" "nginx" {
#   metadata {
#     name = "nginx-example"
#     labels = {
#       App = "nginx"
#     }
#   }

#   spec {
#     container {
#       image = "nginx:1.7.8"
#       name  = "example"

#       port {
#         container_port = 80
#       }
#     }
#   }
# }

# resource "kubernetes_service" "nginx" {
#   metadata {
#     name = "nginx-example"
#   }
#   spec {
#     selector = {
#       App = kubernetes_pod.nginx.metadata[0].labels.App
#     }
#     port {
#       port        = 80
#       target_port = 80
#     }

#     type = "LoadBalancer"
#   }
# }

# output "lb_ip" {
#   value = kubernetes_service.nginx.load_balancer_ingress[0].ip
# }

