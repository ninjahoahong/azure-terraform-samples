# AZURE INFRA

## TERRAFORM
* `terraform init -backend-config=backend-config.tf`
* `terraform plan -var-file=*.tfvar`
* `terraform apply -var-file=*.tfvar`
* `terraform destroy -var-file=*.tfvar`
