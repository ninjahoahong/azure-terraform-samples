variable "subscription_id" {
}

variable "location" {
  description = "The location/region where the core network will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
}

variable "project" {
}

variable "tags" {
  type = map(string)

  default = {
    environment = "dev"
    terraform   = "true"
    owner       = "ninjahoahong"
  }
}

variable "subnet_names" {
  
}

variable "address_space" {
  description = "The address space that is used by the virtual network."
}

variable "dns_servers" {
  description = "The DNS servers to be used with vNet. If not defined then it will used Azure default dns"
}

variable "subnet_prefixes" {
  
}

variable "public_ip_names" {
  
}

variable "vm_names" {
  
}

variable "ssh_key_public" {
  
}

variable "computer_name" {
  
}

variable "vm_admin_username" {
  
}

variable "vm_admin_password" {
  
}

variable "vm_size" {

}

variable "psql_admin_login_user" {

}

variable "psql_admin_login_password" {
  
}

variable "psql_version" {
  
}

variable "psql_sku_name" {
  
}

variable "psql_sku_capacity" {

}

variable "psql_sku_tier" {

}

variable "psql_sku_family" {

}

variable "psql_ssl_enforcement" {

}

variable "psql_storage_mb" {

}

variable "psql_backup_retention_days" {

}

variable "psql_geo_redundant_backup" {

}

variable "managed_disk_type" {

}

variable "ssh_key_path" {
  
}

variable "vm_image_publisher" {

}
variable "vm_image_offer" {

}

variable "vm_image_sku" {

}

variable "vm_image_version" {

}

