provider "azurerm" {
  version         = "=1.28.0"
  subscription_id = var.subscription_id
}

resource "azurerm_resource_group" "resource_group" {
  name     = "${var.project}-rg"
  location = var.location
  tags     = var.tags
}

resource "azurerm_virtual_network" "vnet" {
  name                = "${var.project}-vnet"
  location            = var.location
  address_space       = [var.address_space]
  resource_group_name = azurerm_resource_group.resource_group.name
  dns_servers         = var.dns_servers
  tags                = var.tags
}

resource "azurerm_subnet" "subnets" {
  name                      = "${var.project}-${var.subnet_names[count.index]}"
  virtual_network_name      = azurerm_virtual_network.vnet.name
  resource_group_name       = azurerm_resource_group.resource_group.name
  address_prefix            = var.subnet_prefixes[count.index]
  count                     = length(var.subnet_names)
}

resource "azurerm_network_security_group" "security_group" {
  name                = "${var.project}-sg"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  tags                = var.tags
}

resource "azurerm_network_security_rule" "ssh_access" {
  name                        = "ssh"
  priority                    = 201
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.resource_group.name
  network_security_group_name = azurerm_network_security_group.security_group.name
}

resource "azurerm_network_security_rule" "http_inbound" {
  name                        = "http_inbound"
  priority                    = 301
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.resource_group.name
  network_security_group_name = azurerm_network_security_group.security_group.name
}

resource "azurerm_subnet_network_security_group_association" "network_sg_association" {
  count                     = length(var.subnet_names)
  subnet_id                 = azurerm_subnet.subnets.*.id[count.index]
  network_security_group_id = azurerm_network_security_group.security_group.id
}

resource "azurerm_public_ip" "public_ips" {
  count                        = length(var.public_ip_names)
  name                         = "${var.project}-${var.public_ip_names[count.index]}"
  location                     = var.location
  resource_group_name          = azurerm_resource_group.resource_group.name
  allocation_method            = "Static"
  domain_name_label            = "${var.project}-${var.public_ip_names[count.index]}-label"
  tags                         = var.tags
}

resource "azurerm_network_interface" "vm_nifs" {
  count                     = length(var.vm_names)
  name                      = "${var.vm_names[count.index]}-nif"
  location                  = var.location
  resource_group_name       = azurerm_resource_group.resource_group.name
  tags                      = var.tags

  ip_configuration {
    name                          = "IPConfiguration"
    subnet_id                     = azurerm_subnet.subnets.0.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.public_ips.*.id[count.index]
  }
}

resource "azurerm_virtual_machine" "vms" {
  count                         = length(var.vm_names)
  name                          = var.vm_names[count.index]
  location                      = var.location
  resource_group_name           = azurerm_resource_group.resource_group.name
  network_interface_ids         = [azurerm_network_interface.vm_nifs.*.id[count.index]]
  vm_size                       = var.vm_size
  delete_os_disk_on_termination = true
  tags                          = var.tags

  storage_image_reference {
    publisher = var.vm_image_publisher
    offer     = var.vm_image_offer
    sku       = var.vm_image_sku
    version   = var.vm_image_version
  }

  storage_os_disk {
    name              = "${var.vm_names[count.index]}-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = var.managed_disk_type
  }

  os_profile {
    computer_name  = var.computer_name
    admin_username = var.vm_admin_username
    admin_password = var.vm_admin_password
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = var.ssh_key_path
      key_data = file(var.ssh_key_public)
    }
  }
}

resource "azurerm_postgresql_server" "psql_server" {
  name                = "${var.project}-psql-server"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  tags                = var.tags

  
  sku {
    #[B_Gen4_1 B_Gen4_2 B_Gen5_1 B_Gen5_2 GP_Gen4_2 GP_Gen4_4 GP_Gen4_8 GP_Gen4_16 GP_Gen4_32 GP_Gen5_2 GP_Gen5_4 GP_Gen5_8 GP_Gen5_16 GP_Gen5_32 GP_Gen5_64 MO_Gen5_2 MO_Gen5_4 MO_Gen5_8 MO_Gen5_16 MO_Gen5_32]
    name     = var.psql_sku_name
    capacity = var.psql_sku_capacity
    #[Basic GeneralPurpose MemoryOptimized]
    tier     = var.psql_sku_tier
    family   = var.psql_sku_family
  }

  storage_profile {
    storage_mb            = var.psql_storage_mb
    backup_retention_days = var.psql_backup_retention_days
    geo_redundant_backup  = var.psql_geo_redundant_backup
  }

  administrator_login          = var.psql_admin_login_user
  administrator_login_password = var.psql_admin_login_password
  version                      = var.psql_version
  ssl_enforcement              = var.psql_ssl_enforcement
}
