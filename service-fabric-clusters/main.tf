provider "azurerm" {
  version         = "=1.28.0"
  subscription_id = var.subscription_id
}

resource "azurerm_resource_group" "resource_group" {
  name     = "${var.project}-rg"
  location = var.location
  tags     = var.tags
}

resource "azurerm_service_fabric_cluster" "servicefabric" {
  name                 = "${var.project}-servicefabric"
  resource_group_name  = azurerm_resource_group.resource_group.name
  location             = azurerm_resource_group.resource_group.location
  reliability_level    = "None"
  upgrade_mode         = "Automatic"
  vm_image             = "Linux"
  management_endpoint  = "https://example:80"
  tags                 = var.tags

  node_type {
    name                 = "${var.project}-servicefabric-1"
    instance_count       = 1
    is_primary           = true
    client_endpoint_port = 2020
    http_endpoint_port   = 80
    durability_level     = "Bronze"
  }
}

resource "azurerm_postgresql_server" "psql_server" {
  name                = "${var.project}-psql-server"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  tags                = var.tags

  
  sku {
    #[B_Gen4_1 B_Gen4_2 B_Gen5_1 B_Gen5_2 GP_Gen4_2 GP_Gen4_4 GP_Gen4_8 GP_Gen4_16 GP_Gen4_32 GP_Gen5_2 GP_Gen5_4 GP_Gen5_8 GP_Gen5_16 GP_Gen5_32 GP_Gen5_64 MO_Gen5_2 MO_Gen5_4 MO_Gen5_8 MO_Gen5_16 MO_Gen5_32]
    name     = var.psql_sku_name
    capacity = var.psql_sku_capacity
    #[Basic GeneralPurpose MemoryOptimized]
    tier     = var.psql_sku_tier
    family   = var.psql_sku_family
  }

  storage_profile {
    storage_mb            = var.psql_storage_mb
    backup_retention_days = var.psql_backup_retention_days
    geo_redundant_backup  = var.psql_geo_redundant_backup
  }

  administrator_login          = var.psql_admin_login_user
  administrator_login_password = var.psql_admin_login_password
  version                      = var.psql_version
  ssl_enforcement              = var.psql_ssl_enforcement
}
