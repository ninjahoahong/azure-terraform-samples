output "cluster_id" {
    value = azurerm_service_fabric_cluster.servicefabric.id
}

output "cluster_endpoint" {
    value = azurerm_service_fabric_cluster.servicefabric.cluster_endpoint
}

output "psql_id" {
    value = azurerm_postgresql_server.psql_server.id
}

output "psql_fqdn" {
    value = azurerm_postgresql_server.psql_server.fqdn
}
