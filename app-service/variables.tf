variable "subscription_id" {
}

variable "location" {
  description = "The location/region where the core network will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
}

variable "project" {
}

variable "tags" {
  type = map(string)

  default = {
    environment = "dev"
    terraform   = "true"
    owner       = "ninjahoahong"
  }
}

variable "psql_admin_login_user" {

}

variable "psql_admin_login_password" {
  
}

variable "psql_version" {
  
}

variable "psql_sku_name" {
  
}

variable "psql_sku_capacity" {

}

variable "psql_sku_tier" {

}

variable "psql_sku_family" {

}

variable "psql_ssl_enforcement" {

}

variable "psql_storage_mb" {

}

variable "psql_backup_retention_days" {

}

variable "psql_geo_redundant_backup" {

}

variable "vpn_ip" {

}

