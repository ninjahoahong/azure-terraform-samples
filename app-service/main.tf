provider "azurerm" {
  version         = "=1.28.0"
  subscription_id = var.subscription_id
}

resource "azurerm_resource_group" "resource_group" {
  name     = "${var.project}-rg"
  location = var.location
  tags     = var.tags
}

resource "azurerm_app_service_plan" "app_service_plan" {
  name                = "${var.project}-app-service-plan"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name
  tags                = var.tags

  sku {
    tier = "Basic"
    size = "B1"
  }
}

resource "azurerm_app_service" "app_service" {
  name                = "${var.project}-app-service"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name
  app_service_plan_id = azurerm_app_service_plan.app_service_plan.id
  tags                = var.tags

  site_config {
    linux_fx_version         = "(DOCKER|openjdk:8-alpine)"
    scm_type                 = "LocalGit"
  }

  app_settings = {
    "DATABASE_URL" = azurerm_postgresql_server.psql_server.fqdn
  }
}

resource "azurerm_postgresql_server" "psql_server" {
  name                = "${var.project}-psql-server"
  location            = var.location
  resource_group_name = azurerm_resource_group.resource_group.name
  tags                = var.tags

  
  sku {
    #[B_Gen4_1 B_Gen4_2 B_Gen5_1 B_Gen5_2 GP_Gen4_2 GP_Gen4_4 GP_Gen4_8 GP_Gen4_16 GP_Gen4_32 GP_Gen5_2 GP_Gen5_4 GP_Gen5_8 GP_Gen5_16 GP_Gen5_32 GP_Gen5_64 MO_Gen5_2 MO_Gen5_4 MO_Gen5_8 MO_Gen5_16 MO_Gen5_32]
    name     = var.psql_sku_name
    capacity = var.psql_sku_capacity
    #[Basic GeneralPurpose MemoryOptimized]
    tier     = var.psql_sku_tier
    family   = var.psql_sku_family
  }

  storage_profile {
    storage_mb            = var.psql_storage_mb
    backup_retention_days = var.psql_backup_retention_days
    geo_redundant_backup  = var.psql_geo_redundant_backup
  }

  administrator_login          = var.psql_admin_login_user
  administrator_login_password = var.psql_admin_login_password
  version                      = var.psql_version
  ssl_enforcement              = var.psql_ssl_enforcement
}

resource "azurerm_postgresql_firewall_rule" "allow-vpn" {
  name                = "${azurerm_postgresql_server.psql_server.name}-allow-vpn"
  resource_group_name = azurerm_resource_group.resource_group.name
  server_name         = azurerm_postgresql_server.psql_server.name
  start_ip_address    = var.vpn_ip
  end_ip_address      = var.vpn_ip
}
