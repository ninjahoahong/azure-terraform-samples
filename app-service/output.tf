output "psql_id" {
    value = azurerm_postgresql_server.psql_server.id
}

output "psql_fqdn" {
    value = azurerm_postgresql_server.psql_server.fqdn
}
